package uz.xsoft.galery.view_model;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.xsoft.galery.models.ImageModel;
import uz.xsoft.galery.retrofit.ApiInterface;
import uz.xsoft.galery.retrofit.ApiUtils;

public class CustomViewModel extends ViewModel{
   private MutableLiveData<List<ImageModel>> images = new MutableLiveData<>();
   private ApiInterface imagesInterface= ApiUtils.getApiClient();
   private MutableLiveData<Integer> positionLiveData = new MutableLiveData<>();


   public void setPosition(int position){
       positionLiveData.setValue(position);

   }

   public LiveData<Integer> getPosition(){
        if (positionLiveData.getValue()==null){
            positionLiveData.setValue(0);
        }
       return positionLiveData;
   }

   public void setImages(List<ImageModel> models){
       images.setValue(models);
   }

   public void postImages(List<ImageModel> models){
       images.postValue(models);
   }

   public void loadImages(){

       imagesInterface.getImages().enqueue(new Callback<List<ImageModel>>() {
           @Override
           public void onResponse(Call<List<ImageModel>> call, Response<List<ImageModel>> response) {
               setImages(response.body());
           }

           @Override
           public void onFailure(Call<List<ImageModel>> call, Throwable t) {

           }
       });

   }

   public MutableLiveData<List<ImageModel>> getImages(){
       if (images.getValue() == null){
           loadImages();
       }

       return images;
   }
}

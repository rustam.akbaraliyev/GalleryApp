package uz.xsoft.galery.dialogs;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import uz.xsoft.galery.R;
import uz.xsoft.galery.adapters.DialogAdapter;
import uz.xsoft.galery.models.ImageModel;
import uz.xsoft.galery.view_model.CustomViewModel;


/**
 * Created by User on 2/21/2018.
 */

public class FullDialog extends DialogFragment {
    public  View v;
    private ViewPager list;
    private DialogAdapter dialogAdapter;
    private static List<ImageModel> models;
    private static int po;


    public FullDialog() {
        setStyle(STYLE_NO_FRAME, R.style.FragmentFullScreen);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dialog_pager, container, false);
         list = v.findViewById(R.id.viewpager);
        //dialogAdapter =new DialogAdapter(getChildFragmentManager(),null);
        dialogAdapter = new DialogAdapter(getChildFragmentManager(), ViewModelProviders.of(getActivity()).get(CustomViewModel.class).getImages().getValue());
       // list.setCurrentItem(po);

        list.setAdapter(dialogAdapter);

        list.setCurrentItem(ViewModelProviders.of(getActivity()).get(CustomViewModel.class).getPosition().getValue());
        list.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                            ViewModelProviders.of(getActivity()).get(CustomViewModel.class).setPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return v;
    }


}
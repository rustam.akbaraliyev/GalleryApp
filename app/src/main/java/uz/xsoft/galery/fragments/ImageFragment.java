package uz.xsoft.galery.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import uz.xsoft.galery.R;


public class ImageFragment extends Fragment {

    @SuppressLint("ValidFragment")
    private ImageFragment() {
    }

    public static ImageFragment newInstance(String data){
            ImageFragment imageFragment = new ImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("image","https://picsum.photos/400/400/?" +data);
            imageFragment.setArguments(bundle);
        return  imageFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.item_image,container,false);

        ImageView imageView = v.findViewById(R.id.images);

        imageView.addOnLayoutChangeListener((view, i, i1, i2, i3, i4, i5, i6, i7) -> {

            if (imageView.getWidth()>0){
                Picasso.get().load(getArguments().getString("image")).resize(imageView.getWidth(),imageView.getHeight()).into(imageView);
            }

        });
        return v;
    }
}

package uz.xsoft.galery.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uz.xsoft.galery.R;
import uz.xsoft.galery.adapters.ImageAdapter;
import uz.xsoft.galery.dialogs.FullDialog;
import uz.xsoft.galery.models.ImageModel;
import uz.xsoft.galery.utils.Utils;
import uz.xsoft.galery.view_model.CustomViewModel;


/**
 * Created by phantom on 20.02.2018.
 */

public class ViewPagerFragment extends Fragment implements ImageAdapter.OnChangeListener {

    private RecyclerView list;
    private ImageView connection;
    private ImageAdapter adapter;



    @SuppressLint("ValidFragment")
    private ViewPagerFragment() {

    }

    public static ViewPagerFragment newInstance(int gridType) {
        Bundle bundle = new Bundle();
        bundle.putInt("type", gridType);

        ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
        viewPagerFragment.setArguments(bundle);
        return viewPagerFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        list = view.findViewById(R.id.grid_list);
        connection = view.findViewById(R.id.connection);
        ViewModelProviders.of(getActivity()).get(CustomViewModel.class).setPosition(0);



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        CustomViewModel customViewModel = ViewModelProviders.of(getActivity()).get(CustomViewModel.class);
        ImageAdapter adapter = new ImageAdapter(new ArrayList<>(),getArguments().getInt("type"),getActivity());



        customViewModel.getImages().observe(getActivity(),models -> {
                adapter.changeData(models);
        });
        adapter.setOnChangeListener(this);
        if (!Utils.isOnline(getActivity()))
            Toast.makeText(getContext(), R.string.connection, Toast.LENGTH_SHORT).show();
        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity(), getArguments() != null ? getArguments().getInt("type") : 1);
        list.setAdapter(adapter);
        list.setLayoutManager(manager);

    }


    @Override
    public void onChange(List<ImageModel> models, int position) {

        FullDialog dialog = new FullDialog();
        dialog.show(getFragmentManager(),"ok");
        list.scrollToPosition(ViewModelProviders.of(getActivity()).get(CustomViewModel.class).getPosition().getValue()
        );
    }
}

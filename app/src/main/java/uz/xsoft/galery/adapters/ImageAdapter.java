package uz.xsoft.galery.adapters;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import uz.xsoft.galery.R;
import uz.xsoft.galery.models.ImageModel;
import uz.xsoft.galery.view_model.CustomViewModel;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder> {
    private List<ImageModel> data;
    private int type;
    private OnChangeListener onChangeListener;
    public Context context;

    public ImageAdapter(List<ImageModel> data, int type, Context context) {
        this.context = context;
        this.data = data;
        this.type = type;
    }

    public void changeData(List<ImageModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ImageAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.Holder holder, int position) {

        holder.onBind(data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView author;

        public Holder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.photo);
            author = itemView.findViewById(R.id.name);

        }

        public void onBind(ImageModel model) {

            itemView.setOnClickListener(view -> {
                ViewModelProviders.of((FragmentActivity) context).get(CustomViewModel.class).setPosition(getAdapterPosition());
                if (onChangeListener != null){
                    onChangeListener.onChange(data,getAdapterPosition());
                }

            });

            image.addOnLayoutChangeListener((view, i, i1, i2, i3, i4, i5, i6, i7) -> {
                if (image.getWidth() > 0)
                    Picasso.get().load("https://picsum.photos/400/400/?" + model.getFilename()).resize(image.getWidth(), image.getHeight()).into(image);
            });

            author.setText(model.getAuthor());
        }
    }

    public void setOnChangeListener(OnChangeListener onChangeListener){
        this.onChangeListener = onChangeListener;

    }
    public interface OnChangeListener{
        void onChange(List<ImageModel> models, int position);
    }
}

package uz.xsoft.galery.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uz.xsoft.galery.fragments.ViewPagerFragment;


/**
 * Created by phantom on 20.02.2018.
 */

public class ViewPagerFragmentAdapter extends FragmentPagerAdapter {


    public ViewPagerFragmentAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return ViewPagerFragment.newInstance(1);
        else

            return ViewPagerFragment.newInstance(2);



    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "List";
        else
            return "Grid";

    }

}

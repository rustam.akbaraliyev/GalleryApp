package uz.xsoft.galery.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import uz.xsoft.galery.fragments.ImageFragment;
import uz.xsoft.galery.models.ImageModel;


public class DialogAdapter extends FragmentPagerAdapter {
   private List<ImageModel> data;

    public DialogAdapter(FragmentManager fm, List<ImageModel> data) {
        super(fm);
        this.data=data;

    }



    @Override
    public Fragment getItem(int position) {
        return ImageFragment.newInstance(data.get(position).getFilename());
    }

    @Override
    public int getCount() {
        return data.size();
    }
}

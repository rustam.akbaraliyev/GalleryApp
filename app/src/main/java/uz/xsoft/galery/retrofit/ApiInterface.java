package uz.xsoft.galery.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import uz.xsoft.galery.models.ImageModel;


/**
 * Created by phantom on 01.02.2018.
 */

public interface ApiInterface {
    @GET("list")
    Call<List<ImageModel>> getImages();
}

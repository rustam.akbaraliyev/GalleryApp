package uz.xsoft.galery.retrofit;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by phantom on 03.02.2018.
 */

public class Header {
    private  OkHttpClient.Builder httpClient;
    private  OkHttpClient client;

    public  OkHttpClient getClient(){
        httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder().header("Content-Type","application/json");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        client = httpClient.build();
        return client;
    }
}

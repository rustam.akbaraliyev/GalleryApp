package uz.xsoft.galery.retrofit;

/**
 * Created by phantom on 01.02.2018.
 */

public class ApiUtils {
    public static final String BASE_URL="https://picsum.photos/";
    public static ApiInterface getApiClient()
    {
        return ApiClient.getClient(BASE_URL).create(ApiInterface.class);
    }

}
